function whatIsBetter() {
  var things = ["Interessen", "Branchen"]
  // get active sheet Name
  var sheetName = SpreadsheetApp.getActive().getActiveSheet().getName();
  // check if sheet is A OR B sheet
  var aOrBPos = sheetName.indexOf("A OR B");
  if(aOrBPos !== -1) {
    var thing = things.find((thing) => sheetName.indexOf(thing) !== -1)
    if(thing !== undefined) {
      // find the control sheet
      var controlSheet = SpreadsheetApp.getActive().getSheetByName(thing + " A OR B");
      // find the datasheet, which contains the list
      var dataSheet = SpreadsheetApp.getActive().getSheetByName(thing);
      // garther all data from the list
      var data = dataSheet.getDataRange().getValues();
      // create an array which will contains all random values
      var randoms = [Math.floor((Math.random() * 100))];
      // create a counter variable for the random array
      var i = 0;
      // loop over the random array until all values are set
      while(i < 2) {
        // check if the value is the title or out of the datarange.
        // also check if we have two times the same value
        if(randoms[i] < 2 || randoms[i] > (data.length) || (randoms.length > 1 && randoms[i] == randoms[i-1])) {
          // if the value is not valid create a new values
          randoms[i] = Math.floor(Math.random() * 100);
        } else {
          // if the values is valid create a new value and add it to the randoms array
          // also add one to the counter
          i++;
          randoms.push(Math.floor(Math.random() * 100));
        }
      }
      // log the randoms array for debugging
      Logger.log('Randoms: ' + randoms);
      // get all relevant data from the controlsheet
      var a = controlSheet.getRange("A1:B2").getValues();
      // loop over all data stored in the datasheet
      for (i = 0; i < data.length; i++) {
        // check if the current data is equal to the first item in the control sheet
        if(data[i][0] == a[0][0]) {
          // log the current datanumber and field for debugging
          Logger.log("i = " + i + " Datafield A: " + a[0][0]);
          // check if the field has an x or X set
          if(a[1][0] == "x" || a[1][0] == "X") {
            // if yes give it a point
            dataSheet.getRange(i+1, 2).setValue(data[i][1] + 1);
          // check if the other one has a x or X set
          } else if(a[1][1] == "x" || a[1][1] == "X") {
            // if yes subtract a point
            dataSheet.getRange(i+1, 2).setValue(data[i][1] - 1);
          }
          // if no point was set, do nothing
        // check if the current data is equal to the second item in the control sheet
        } else if (data[i][0] == a[0][1]) {
          // log the current datanumber and field for debugging
          Logger.log("i = " + i + " Datafield B: " + a[0][1]);
          // check if the field has an x or X set
          if(a[1][1] == "x" || a[1][1] == "X") {
            // if yes give it a point
            dataSheet.getRange(i+1, 2).setValue(data[i][1] + 1);
          // check if the other one has a x or X set
          } else if(a[1][0] == "x" || a[1][0] == "X"){
            // if yes subtract a point
            dataSheet.getRange(i+1, 2).setValue(data[i][1] - 1);
          }
          // if no point was set, do nothing
        }
      }
      // always set the new variables to compare
      controlSheet.getRange("A1").setValue("=" + thing + "!A" + randoms[0]);
      controlSheet.getRange("B1").setValue("=" + thing + "!A" + randoms[1]);
      // reset the point fields
      controlSheet.getRange("A2").setValue("");
      controlSheet.getRange("B2").setValue("");
    }
  }
}
